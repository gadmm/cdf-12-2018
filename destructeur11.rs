#![allow(unused)]
#![feature(nll)]
// Un protocole simple
//
// 1) bind
//   2) connect
//     3) send/receive
//   4) close
// 5) unbind
//
// Ce principle est utilisé pour les sockets Berkeley dans
// std::net::TcpListener et std::net::TcpStream.

// Client
struct Client {
    name: &'static str,
}

impl Client {
    fn bind(name: &'static str) -> Client {
        println!("< Binding {}", name);
        Client { name: name }
    }
    fn connect<'a>(&'a self, name: &'static str) -> Connection<'a> {
        println!("< Connecting {} for client {}", name, self.name);
        Connection {
            client: &self,
            name: name,
        }
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        println!("> Unbinding {}", self.name);
    }
}

// Connexion
struct Connection<'a> {
    client: &'a Client,
    name: &'static str,
}

impl<'a> Connection<'a> {
    fn send(&self) {
        println!("{} sending", self.name);
    }
    fn recv(&self) {
        println!("{} receive", self.name);
    }
}

impl<'a> Drop for Connection<'a> {
    fn drop(&mut self) {
        println!("> Disconnect {} for client {}", self.name, self.client.name);
    }
}

fn main() {
    let client = Client::bind("A");
    let connection = client.connect("1");
    //drop(connection);
    connection.send();
    //drop(client);
}
