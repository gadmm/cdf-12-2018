#include <iostream>
using namespace std;

int next_i = 0;

struct Ressource {
  int i;
  // constructeur
  Ressource() : i(next_i++) {
    cout << "création de la ressource " << i << endl;
  }
  // destructeur
  ~Ressource() {
    cout << "destruction de la ressource " << i << endl;
  }
  void utiliser() {
    cout << "utilisation de la ressource " << i << endl;
  }
};
