#include "ressource.h"
#include <memory>

// Déplacer les ressources (C++11)

void f(unique_ptr<Ressource> a)
{
  a->utiliser();
}

int main()
{
  auto a0 = make_unique<Ressource>();
  auto a1 = make_unique<Ressource>();
  f(move(a0));
  cout << "fin de portée" << endl;
}
