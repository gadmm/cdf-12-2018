#include "ressource.h"

// Création et destruction automatiques

void f(Ressource & a)
{
  a.utiliser();
}

int main()
{
  Ressource a0;
  Ressource a1;
  f(a0);
  cout << "fin de portée" << endl;
}

