// 11. Invalidation d'itérateur (Rust)

fn main() {
    let mut vec = vec!["a", "b", "c"];
    let first = &mut vec[0];
    vec.push("e");
    let mut _vec2 = vec!["f", "g", "h"];
    println!("vec[0] contient {}", first);
}
