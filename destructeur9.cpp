#include <iostream>
#include <vector>
using namespace std;

// Itérateurs

int main()
{
  vector<string> vec{"a", "b", "c"};
  string & first = vec[0];
  first = "d";
  //vec.push_back("e"); // Invalidation d'itérateur
  vector<string> vec2{"f", "g", "h"};
  cout << "vec[0] contient " << first;
  return 0;
}
