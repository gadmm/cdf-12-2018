Code de démonstration du séminaire « Peut-on dupliquer un objet ?
Linéarité et contrôle des ressources » donné le 19 décembre 2018 au
Collège de France.

Compilation et exécution C++11:
```
g++ destructeur1.cpp
./a.out
```
(etc.)

Compilation et exécution Rust:
```
rustc destructeur10.rs
./destructeur10
```
(etc.)