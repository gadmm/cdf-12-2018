#include "ressource.h"
#include <memory>
#include <vector>

// Structures de données—ressources

void f(vector<unique_ptr<Ressource>> vec)
{
  cout << "utilisation de vec" << endl;
}

int main()
{
  auto a0 = make_unique<Ressource>();
  auto a1 = make_unique<Ressource>();
  vector<unique_ptr<Ressource>> vec;
  vec.push_back(move(a1));
  vec.push_back(move(a0));
  f(move(vec));
  cout << "fin de portée" << endl;
  return 0;
}
