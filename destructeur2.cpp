#include "ressource.h"
#include <iostream>
using namespace std;

// Exceptions

void f_avec_exception(Ressource & a)
{
  a.utiliser();
  throw runtime_error("f_avec_exception");
}

int main()
{
  try {
    Ressource a;
    f_avec_exception(a);
    cout << "fin de portée" << endl;
  } catch (runtime_error err) {
    cout << "exception: " << err.what() << endl;
  }
}
